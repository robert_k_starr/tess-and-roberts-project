# Files
FILE	:= bin/
EXEC	:= $(FILE)lattice
SRC		:= $(wildcard src/*.cpp)
OBJ		:= $(patsubst %.cpp, %.o, $(SRC))
HPP		:= $(wildcard src/*.hpp)
DATA	:= $(wildcard *.txt) $(wildcard Files/*.txt)
FILEDIR	:= src/


# Options
CC		:= g++
CFLAGS	:= 
LDFLAGS	:=

BLAS	:=
INC		:=
LDLIBS	:=

# Rules
standard: $(EXEC)

run: $(EXEC)
	./$(EXEC)

$(EXEC): $(OBJ)
	mkdir -p $(FILE)
	$(CC) $(BLAS) $(INC) $(LDFLAGS) $(LDLIBS) -o $@ $^

$(FILEDIR)%.o: $(FILEDIR)%.cpp
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@
	
# Phony targets
.PHONY: clobber clean neat echo

clobber: clean
	$(RM) -r $(FILE)
	
clean:	neat
	$(RM) $(OBJ)
neat:
	$(RM) $(DATA) *~ .*~
echo:
	@echo $(OBJ)