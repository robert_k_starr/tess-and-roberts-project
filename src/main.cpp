#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "main.hpp"
#include "functions.hpp"
#include "output.hpp"

using namespace std;

double xmin, xmax, ymin, ymax, zmin, zmax;

string filename = "Files/dataoutput.txt";

int main(){
	cleanfile(filename);
	
	for (int i = 0; i < XATOMS; i++){
		for (int j = 0; j < YATOMS; j++){
		  double x[3] = {i, j, 0};
		  convert(x);
		  new atom(x);
		}
	}
	
	xmin = 0;
	ymin = 0;
	zmin = 0;
	
	double coords[3] = {XATOMS, YATOMS, ZATOMS};
	convert(coords);
	
	xmax = coords[0];
	ymax = coords[1];
	zmax = coords[2];
	
	for (int i = 0; i < XATOMS-1; i++){ 	        
	        for (int j = 0; j < YATOMS-1; j++){
		    int r = j%2;
		    bond(&atomarray[i][j], &atomarray[i][j+1]); // bond with atom immediately above
		    bond(&atomarray[i][j], &atomarray[i+1][j]); // bond with atom immediately adjacent (same y value)		  
		    if (r == 0 && i != 0) {
		      bond(&atomarray[i][j], &atomarray[i-1][j+1]); // for even j, bond to upper left atom
		    }
		    else if (r == 1) {
		      bond(&atomarray[i][j], &atomarray[i+1][j+1]); // for odd j, bond to upper right atom
		    }
		}
		bond(&atomarray[i][YATOMS-1], &atomarray[i+1][YATOMS-1]);  // for topmost atom, only bond to adjacent atom
	}
 
	// Bond right edge to atoms above and diagonally for even y values
	for (int j = 0; j < YATOMS-1; j++) {
	       int r = j%2; 
	       bond(&atomarray[XATOMS-1][j], &atomarray[XATOMS-1][j+1]);
	       if (r == 0) {
		 bond(&atomarray[XATOMS-1][j], &atomarray[XATOMS-2][j+1]); // for even j, bond to upper left atom
	       }
	}
	
	printf("atom count = %i\n", atomcount);
	printf("bond count = %i\n", bondcount);
	
	walk();

	initialize(filename);
	outputtext(filename, "Atoms\n\n");
	
	for (int i = 0; i < XATOMS; i++) {
		for (int j = 0; j < YATOMS; j++) outputatom(filename, atomarray[i][j]);
	}

	outputtext(filename, "\nBonds\n\n");
	
	for (int i = 0; i < bondcount; i++) outputbond(filename, bondarray[i]);
	
	printf("Counts after walking:\n");
	printf("atom count = %i\n", atomcount);
	printf("bond count = %i\n", bondcount);
	
	return 0;
}
