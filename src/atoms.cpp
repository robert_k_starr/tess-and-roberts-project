#include <iostream>
#include <stdio.h>

#include "atoms.hpp"
#include "functions.hpp"

using namespace std;

// Atom can be initialized with either an array of the coordinates or
// individual x, y, z values

atom::atom(double x, double y, double z){
	init(x, y, z);
}

atom::atom(double *value){
	init(value[0], value[1], value[2]);
}

atom::atom(){
	for (int i = 0; i < 3; i++) coords[i] = -1;
	realatom = false;
}

void atom::init(double x, double y, double z){
	coords[0] = x;
	coords[1] = y;
	coords[2] = z;
	realatom = true;
	openbond = 0;
	id = atomcount + 1;
	addatom(this);
}

void atom::printcoords(){
	printf("\n(");
	for (int i = 0; i < 3; i++) printf("%f, ", coords[i]);
	printf("\b\b)\n");
}

int atom::giveid(){
	return id;
}

void atom::bond(int aid, int bid){
    //bonds[openbond++] = atom1;
    bonds[openbond] = aid; 
	bondid[openbond++] = bid;
}

void atom::breakbond(int brokenid) { // Is this completely right? I don't think so...
	for (int i = 0; i < openbond; i ++){
		if (bonds[i] == brokenid) bonds[i] = bonds[openbond - 1];
	}
	openbond--;
}

void atom::returnbonds(int *dummy){
	for (int i = 0; i < openbond; i++) dummy[i] = bondid[i];
	for (int i = openbond; i < 6; i++) dummy[i] = -1;
}

bool atom::empty(){
	return not realatom; 	// Returns true if atom is empty
}

void atom::replacebond(int oldvalue, int newvalue){
	for (int i = 0; i < openbond; i++){
		if (bondid[i] == oldvalue) bondid[i] = newvalue;
	}
}

void atom::givecoords(double * x){
	for (int i = 0; i < 3; i++) x[i] = coords[i];
}

int atom::checkhighlow(int minid, bool high){
	int count = 0, highlow;
	if (high) highlow = 1;
	else highlow = -1;

	for (int i = 0; i < openbond; i++){
		if (highlow*minid < highlow*bonds[i]) count++;
	}
	
	return count;
}

// Bond two atoms together

bond::bond(atom *atom1, atom *atom2){
    pair[0] = atom1;
	pair[1] = atom2;

    int aid1 = atom1->giveid();
    int aid2 = atom2->giveid();

	broken = false; 
	id = bondcount + 1;
	//cout << " id in bond = " << id; 

	atom1->bond(aid2, id);	
	atom2->bond(aid1, id);
	
	addbond(this);	
}

bond::bond(){
}

bool bond::status(){
	return broken; 
}
 
int bond::giveid(){
	return id;
}
 
void bond::giveids(int * ids){
	ids[0] = pair[0]->giveid();
	ids[1] = pair[1]->giveid();
}

void bond::breakbond(atom *atom1, atom *atom2) {
    //broken = true;
	atom1->breakbond(atom2->giveid());
	atom2->breakbond(atom1->giveid()); 

	removebond(id); //this lowers bond count by 1
} 

void bond::setid(int value){
	if (id != 0){
		pair[0]->replacebond(id, value);
		pair[1]->replacebond(id, value);
		}
	
	id = value;
}

bool bond::highodds(){
	// First check to see if the bond is "diagonal" rather than "flat"
	
	double coords1[3], coords2[3];
	int minid, highorlow = -1;
	
	// Set to true if first coords have higher y-value
	
	bool first_higher = false;
	
	pair[0]->givecoords(coords1);
	pair[1]->givecoords(coords2);
	
	if (coords1[0] == coords2[0]) return false;
	
	// Check to see if they have broken adjacent bonds
	
	if (coords1[1] > coords2[1]){
		first_higher = true;
		highorlow = 1;
	}
	
	minid = pair[0]->giveid() + highorlow*2;
	
	if (pair[0]->checkhighlow(minid, first_higher) < 2) return true;
	
	minid = pair[1]->giveid() + -highorlow*2;
	
	if (pair[1]->checkhighlow(minid, (not first_higher)) < 2) return true;
	
	return false;
	
}