#include <iostream>
#include <stdio.h>

#include "output.hpp"
#include "parameters.hpp"

using namespace std;

void outputtext(string filename, string text){
	ofstream file;
	
	file.open(filename, ios::app);
	file << text;
	file.close();
}

void outputtext(string filename, double number){
	string temp;
	ostringstream convert;
	convert << number;
	
	temp = convert.str();
	
	outputtext(filename, temp);
}

void outputtext(string filename, double *number, int n){
	int i;
	for (i = 0; i < n; i++){
		outputtext(filename, number[i]);
		outputtext(filename, " ");
	}
	outputtext(filename, "\n");
}

void outputtext(string filename, int number){
	outputtext(filename, (double) number);
}

void cleanfile(string filename){
	ofstream file;
	file.open(filename);
	file.close();
}

void combinefile(string file1, string file2){
	ifstream infile(file1);
	string line;
	while (getline(infile, line)) {
		outputtext(file2, line);
		outputtext(file2, "\n");
	}
}

int outputatom(string file, atom atom1){ 	// Need to do this as int rather than void 
											// to make sure we're not outputting fake atoms
//	if (atom1.empty()) return 0;
//	if (atom1.empty()) outputtext(file, "ERROR\n");	
	double data[6], coords[3];
	data[0] = (double) atom1.giveid();
	data[1] = 0;			// Molecule ID equals 0 always for this simulation
	data[2] = 1; 			// Atom type equals 0 always for this simulation
	
	atom1.givecoords(coords);
	
	for (int i = 0; i < 3; i++) data[3 + i] = coords[i];
	
	outputtext(file, data, 6);
	
	return 1;
}

int outputbond(string file, bond bond1){
	//if (bond1.status()) return 0;
	
	double data[4];
	int ids[2];
	
	data[0] = (double) bond1.giveid();
	data[1] = 1;
	
	bond1.giveids(ids);
	
	data[2] = (double) ids[0];
	data[3] = (double) ids[1];
	
	outputtext(file, data, 4);
	
	return 1;
}

void initialize(string file){
	outputtext(file, "LAMMPS Data File \n\n");
	outputtext(file, atomcount);
	outputtext(file, " atoms\n");
	outputtext(file, bondcount);
	outputtext(file, " bonds\n");
	outputtext(file, "\n");
	
	outputtext(file, "3 atom types\n");
	outputtext(file, "3.0 bond types\n\n");
	
	outputtext(file, xmin - BUFFER);
	outputtext(file, " ");
	outputtext(file, xmax + BUFFER);
	outputtext(file, " xlo xhi\n");
	
	outputtext(file, ymin - BUFFER);
	outputtext(file, " ");
	outputtext(file, ymax + BUFFER);
	outputtext(file, " ylo yhi\n");
	
	outputtext(file, zmin - BUFFER);
	outputtext(file, " ");
	outputtext(file, zmax + BUFFER);
	outputtext(file, " zlo zhi\n\n");
}