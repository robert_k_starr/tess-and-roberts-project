#ifndef atoms_hpp
#define atoms_hpp

#include <iostream>
#include <stdio.h>

using namespace std;

extern int atomcount, bondcount;

class atom{
	double coords[3];
        //atom *bonds[6];
		//try including a pointer to the bonds?
        int bonds[6]; 
        int bondid[6]; 
	bool realatom;
	int openbond;
	int id;
  
	
	public:
	atom();
	atom(double *);
	atom(double, double, double);
	
	void init(double, double, double);
	bool empty();
  
    void bond(int, int);
    void returnbonds(int *);
    void breakbond(int);
  	
	void printcoords();
	void givecoords(double *);
	int giveid();
	void replacebond(int, int);
	
	int checkhighlow(int, bool);
};

class bond{
	int id;
        bool broken; 
	atom *pair[2];
	
	public:
	
	bond(atom *, atom *);
	bond();
    bool status();
    bool highodds();
	int giveid();
	void giveids(int *);
    void breakbond(atom *, atom *); 
    void setid(int);
};

#endif /* atoms_hpp */
