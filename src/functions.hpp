#ifndef functions_hpp
#define functions_hpp

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "atoms.hpp"
#include "parameters.hpp"

void convert(double *);
double distance(double *, double *);
double distance(atom *, atom *);

void addatom(atom *);
void addbond(bond *);
void removebond(int);
int matchbond(atom *, atom *);

bool sharedatom(atom *, atom *);

void walk(); 

#endif /* functions_hpp */
