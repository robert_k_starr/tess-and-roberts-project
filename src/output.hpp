#ifndef output_hpp
#define output_hpp

#include <iostream>
#include <stdio.h>

#include <fstream>
#include <cstring>
#include <sstream>

#include "atoms.hpp"

using namespace std;

extern int atomcount, bondcount;

extern double xmax, ymax, zmax, xmin, ymin, zmin;

void outputtext(string, string);
void outputtext(string, double);
void outputtext(string, double *, int);
void outputtext(string, int);
int outputatom(string, atom);
int outputbond(string, bond);

void cleanfile(string);
void combinefile(string, string);

void initialize(string);

#endif /* output_hpp */