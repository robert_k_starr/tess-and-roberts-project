#ifndef main_hpp
#define main_hpp

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "atoms.hpp"
#include "parameters.hpp"

extern int atomcount, bondcount;

extern atom atomarray[XATOMS][YATOMS];		// Total atoms = XATOMS*YATOMS
extern bond bondarray[6*XATOMS*YATOMS/2];	// Is my math here correct?

#endif /* main_hpp */
