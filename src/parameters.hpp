#ifndef parameters_hpp
#define parameters_hpp

#include <iostream>
#include <stdio.h>

#define XATOMS 200
#define YATOMS 200
#define ZATOMS 1

#define BUFFER 1

#define SPACING 1

#define TMAX 500         //defines iterations for walker
#define BASEODDS  0.0002        //defines percent odds of breaking a bond
#define INCREASE 20     //defines the factor for increased odds of having adjacent bond broken

#endif /* parameters_hpp */
