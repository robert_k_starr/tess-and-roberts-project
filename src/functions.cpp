#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "functions.hpp"
#include "parameters.hpp"
#include "atoms.hpp"

using namespace std;

int i, j;

int atomcount = 0, bondcount = 0;

atom atomarray[XATOMS][YATOMS];		// Total atoms = XATOMS*YATOMS
bond bondarray[6*XATOMS*YATOMS/2];	// 6 Bonds per atom divided by 2 to eliminate duplicates

void convert(double *values){
	values[0] = (values[0] + (((int) values[1])%2)/2.0)*SPACING;
	values[1] = (values[1]*sqrt(3)/2.0)*SPACING;
	values[2] = (values[2])*SPACING;
}

double distance(double *x, double *y){
	double sum = 0;
	for (i = 0; i < 3; i++) sum = sum + pow(x[i] - y[i], 2);
	return sqrt(sum);
}

double distance(atom *atom1, atom *atom2){

	double x[3], y[3];
	atom1->givecoords(x);
	atom2->givecoords(y);

	return distance(x, y);
}

void addatom(atom *atom1){
    int i = atomcount/YATOMS;
	int j = atomcount%YATOMS;
    atomarray[i][j] = *atom1;
	atomcount++;
}

void addbond(bond *bond1){
	bondarray[bondcount] = *bond1;
	bondcount++;
}

void removebond(int id){
	bondcount--;
	bondarray[id - 1] = bondarray[bondcount];
	bondarray[id - 1].setid(id);
}

int matchbond(atom *atom1, atom *atom2){
	int bonds1[6], bonds2[6], id;

	atom1->returnbonds(bonds1);
	atom2->returnbonds(bonds2);

	for (int i = 0; i < 6; i++){
		if (bonds1[i] < 0) break;
		for (int j = 0; j < 6; j++){
			if (bonds2[j] < 0) break;
			if (bonds1[i] == bonds2[j]) return bonds1[i];
		}
	}
	return -1;
}

bool sharedatom(atom *atom1, atom *atom2){
	int bonds1[6], bonds2[6];

	atom1->returnbonds(bonds1);
	atom2->returnbonds(bonds2);

	for (int i = 0; i < 6; i++){
		for (int j = i; j < 6; j++){
			if ((bonds1[i] == bonds2[j])||(bonds1[j] == bonds2[i])) return true;
		}
	}

	return false;
}

void walk() {

  cout << "Now start walking... \n";

  //Start with atom in middle of lattice
  int startx = XATOMS/2;
  int starty = YATOMS/2;

  //Indices for walker's previous atom and new atom
  int i_old, j_old, i_new, j_new;

  //Initialize random seed
  srand(time(NULL));
  int r1;
  double r2;

  i_old = startx;
  j_old = starty;
  //cout << "startx = " << startx << " starty = " << starty << " \n";

  //Some loop for the walker.  I assume lattice is large enough that walker won't hit edge.
  // ^ This assumption needs to be delt with, I think, at some point
  for(int t = 0; t < TMAX; t++) {

    //"Roll the dice." This determines the direction.
    r1 = (int)(rand()%6);
    //cout << "for t = " << t << " r1 = " << r1 << "\n";

    if(r1 == 0){
      i_new = i_old+1;
      j_new = j_old;
    }
    if(r1 == 1) {
      i_new = i_old-1;
      j_new = j_old;
    }
    if(r1 == 2) {
      i_new = i_old;
      j_new = j_old+1;
    }
    if(r1 == 3){
      i_new = i_old;
      j_new = j_old-1;
    }
    if(r1 == 4) {
      if(j_old%2 == 0) {
	i_new = i_old-1;
	j_new = j_old+1;
      }
      else {
	i_new = i_old+1;
	j_new = j_old+1;
      }
    }
    if(r1 == 5) {
      if(j_old%2 == 0) {
	i_new = i_old-1;
	j_new = j_old-1;
      }
      else {
	i_new = i_old+1;
	j_new = j_old-1;
      }
    }

	if ((i_new < 0) || (j_new < 0) || (i_new > XATOMS - 1) || (j_new > YATOMS - 1)){
	  r1 = int(rand()%6 - 3);
	  i_old = startx + r1;
	  r1 = int(rand()%6 - 3);
	  j_old = starty + r1;

		printf("%i/%i\n", t, TMAX);

		continue;
	}
    // Now that we've got the direction, lets test the bond

    int atom_ids[2];
    int aid1, aid2, bid;
    aid1 =  atomarray[i_old][j_old].giveid();
    aid2 =  atomarray[i_new][j_new].giveid();
    //cout << aid1 << "," << aid2 << "\n";

	bid = matchbond(&atomarray[i_old][j_old], &atomarray[i_new][j_new]) - 1;

    //cout << "bid = " << bid;
    //cout << " give id = " << atom_ids[0] << "," << atom_ids[1] << "\n";

    if(bid >= 0){
	    bondarray[bid].giveids(atom_ids);

		//if ((atom_ids[0] != aid1)&&(atom_ids[0] != aid2)) printf("bond = %i\naid1 = %i\naid2 = %i\natom1 = %i\natom2 = %i\n\n", bid, aid1, aid2, atom_ids[0], atom_ids[1]);

      r2 = ((double) rand() / (RAND_MAX));
      //cout << "for t = " << t << " r2 = " << r2 << "\n";
	double odds = BASEODDS;

	// Test for nearby broken bonds. If true, increase odds.
	bool highodds = bondarray[bid].highodds();

	//get number of bonds for current atom? If some bonds area already broken, does that increase the odds?
	//check nearby bonds. If bonds in same row are broken, increase the odds?

	if (highodds) {
	  odds = INCREASE*BASEODDS;
	}

	if(r2 <= odds) {  //If less than ODDS, break the bond and return to starting atom
	  //cout << "Break this bond. \n";

	  bondarray[bid].breakbond(&atomarray[i_old][j_old], &atomarray[i_new][j_new]);

	  // After breaking, return to position near center, +/-t above/below and to right/left of starting atom
	  r1 = int(rand()%6 - 3);
	  i_old = startx + r1;
	  r1 = int(rand()%6 - 3);
	  j_old = starty + r1;
	}
	else {  //If bond doesn't break, start over from the new atom.
	  i_old = i_new;
	  j_old = j_new;
	  t--;
	}
    }
    else t--;
      //If there's no bond, keep same i_old and j_old and "roll the dice again."
  }

	cout << "Done walking!\n";
}
